package me.hikari.itoupai

import io.objectbox.annotation.Entity
import io.objectbox.annotation.Id
import io.objectbox.annotation.Unique

@Entity
data class Photo(
    @Id var id: Long = 0,
    var name: String = "",
    @Unique var path: String = "",
    var upload: Int = 0
)