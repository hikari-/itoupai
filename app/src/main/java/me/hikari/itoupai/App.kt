package me.hikari.itoupai

import android.app.Application
import me.hikari.itoupai.util.ObjectBox

class App : Application() {

    override fun onCreate() {
        super.onCreate()
        ObjectBox.init(this)
    }
}