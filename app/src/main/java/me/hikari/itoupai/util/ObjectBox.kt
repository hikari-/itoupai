package me.hikari.itoupai.util

import android.content.Context
import io.objectbox.BoxStore
import me.hikari.itoupai.MyObjectBox

object ObjectBox {
    lateinit var boxStore: BoxStore

    fun init(context: Context) {
        boxStore = MyObjectBox.builder()
            .androidContext(context.applicationContext)
            .build()
    }

    fun get(): BoxStore {
        return boxStore
    }
}