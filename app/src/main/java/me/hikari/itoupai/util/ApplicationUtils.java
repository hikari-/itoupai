package me.hikari.itoupai.util;

import android.annotation.SuppressLint;
import android.app.Application;

import androidx.annotation.NonNull;

import java.lang.reflect.Field;
import java.lang.reflect.Method;

@SuppressLint("PrivateApi")
public final class ApplicationUtils {
    private ApplicationUtils() {
        throw new AssertionError("No instances.");
    }

    private static Application sApplication;

    static {
        try {
            Class<?> activityThread = Class.forName("android.app.ActivityThread");
            Method m_currentActivityThread = activityThread.getDeclaredMethod("currentActivityThread");
            Field f_mInitialApplication = activityThread.getDeclaredField("mInitialApplication");
            f_mInitialApplication.setAccessible(true);
            Object current = m_currentActivityThread.invoke(null);
            Object app = f_mInitialApplication.get(current);
            sApplication = (Application) app;
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 获取全局Application
     *
     * @return
     */
    public static Application getApplication() {
        return sApplication;
    }

    /**
     * 设置全局Application
     *
     * @param application
     */
    public static void setupApplication(@NonNull Application application) {
        sApplication = application;
    }
}
