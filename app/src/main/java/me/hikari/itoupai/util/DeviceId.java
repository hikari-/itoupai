package me.hikari.itoupai.util;

import android.provider.Settings;

import java.net.NetworkInterface;
import java.net.SocketException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import kotlin.UByte;

public final class DeviceId {
    private static String mDeviceId = null;

    private DeviceId() {
    }

    public static String getDeviceID() {
        if (mDeviceId == null) {
            initDeviceID();
        }
        return mDeviceId;
    }

    public static String toMd5(byte[] abyte0, boolean flag) {
        try {
            MessageDigest messagedigest = MessageDigest.getInstance("MD5");
            messagedigest.reset();
            messagedigest.update(abyte0);
            return toHexString(messagedigest.digest(), "", flag);
        } catch (NoSuchAlgorithmException e) {
            return "";
        }
    }

    public static String toHexString(byte[] abyte0, String s, boolean flag) {
        StringBuilder stringbuilder = new StringBuilder();
        for (byte byte0 : abyte0) {
            String s1 = Integer.toHexString(byte0 & UByte.MAX_VALUE);
            if (flag) {
                s1 = s1.toUpperCase();
            }
            if (s1.length() == 1) {
                stringbuilder.append("0");
            }
            stringbuilder.append(s1);
            stringbuilder.append(s);
        }
        return stringbuilder.toString();
    }

    private static void initDeviceID() {
        String mac = getMacAddress();
        String aid = getAndroidId();
        StringBuilder sb = new StringBuilder();
        sb.append(mac);
        sb.append(aid);
        mDeviceId = toMd5(sb.toString().getBytes(), true);
    }

    private static String getMacAddress() {
        String str = "02:00:00:00:00:00";
        try {
            StringBuilder buf = new StringBuilder();
            try {
                NetworkInterface networkInterface = NetworkInterface.getByName("eth1");
                if (networkInterface == null) {
                    networkInterface = NetworkInterface.getByName("wlan0");
                }
                if (networkInterface == null) {
                    return str;
                }
                for (byte b : networkInterface.getHardwareAddress()) {
                    buf.append(String.format("%02X:", new Object[]{Byte.valueOf(b)}));
                }
                if (buf.length() > 0) {
                    buf.deleteCharAt(buf.length() - 1);
                }
                return buf.toString();
            } catch (SocketException e) {
                e.printStackTrace();
                return str;
            }
        } catch (Exception e2) {
            return str;
        }
    }

    private static String getAndroidId() {
        try {
            return Settings.Secure.getString(ApplicationUtils.getApplication().getContentResolver(), "android_id");
        } catch (Exception e) {
            return "000000";
        }
    }
}
